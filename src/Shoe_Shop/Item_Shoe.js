import React, { Component } from 'react'

export default class Item_Shoe extends Component {
    render() {
        let { image, name, price } = this.props.shoe
        return (
            <div className="col-4">
                <div className="card h-100">
                    <img src={image} className="card-img-top" alt="..." />
                    <div className="card-body">
                        <h5 className="card-title">{name}</h5>
                        <p className="card-text">{price}</p>
                        <a
                            onClick={() => { this.props.handleAddCard(this.props.shoe) }}
                            href="#"
                            className="btn btn-primary"
                        >
                            Add
                        </a>
                    </div>
                </div>
            </div>
        )
    }
}
