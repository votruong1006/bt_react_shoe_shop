import React, { Component } from 'react'
import Card_Shoe from './Card_Shoe'
import { data_shoe } from './data_shoe'
import List_Shoe from './List_Shoe'

export default class Shoe_Shop extends Component {
    state = {
        listShoe: data_shoe, cardShoe: []
    }
    handleAddCard = (shoe) => {
        let cloneCard = [...this.state.cardShoe]
        let index = cloneCard.findIndex((item) => {
            return item.id == shoe.id
        })
        if (index == -1) {
            let newShoe = { ...shoe, soLuong: 1 }
            cloneCard.push(newShoe)
        } else {
            cloneCard[index].soLuong++
        }
        this.setState({
            cardShoe: cloneCard
        })
    }
    handleDelete = (idshoe) => {
        let newCard = this.state.cardShoe.filter((item) => {
            return item.id != idshoe

        })
        this.setState({ cardShoe: newCard })
    }
    handleChangeQuatity = (idShoe, soLuongg) => {
        let cloneCard = [...this.state.cardShoe]
        let index = cloneCard.findIndex((item) => {
            return item.id == idShoe
        })
        cloneCard[index].soLuong = cloneCard[index].soLuong + soLuongg
        this.setState(
            { cardShoe: cloneCard }
        )
    }
    render() {
        return (
            <div className="container">
                <div className='row'>
                    {this.state.cardShoe.length > 0 && <div className="col-8"><Card_Shoe handleChangeQuatity={this.handleChangeQuatity} handleDelete={this.handleDelete} card={this.state.cardShoe} /></div>}
                    <div className="col-4"><List_Shoe handleAddCard={this.handleAddCard} list={this.state.listShoe} /></div>
                </div>
            </div>
        )
    }
}
